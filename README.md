## SDN-Topology Discovery using POX v1.0
**This project was created within the context of my Master Thesis: "Topology Management for SDN-based IoT Architectures" for the MSc "Information Systems" @ AUEB**

**Abstract**

SDNs promise new capabilities, transforming each network from a simple packet forwarding system into an intelligent distribution medium. In the recent work entitled "Edge-Assisted Traffic Engineering and Applications in the IoT", the functionality of the basic element of SDN, the controller, is extended in order to perform route calculations on given topologies.

In particular, Nikos Fotiou et al., exploit the use of tags that describe the properties and capabilities of network nodes by enabling a new type of network control applications and thus connecting user applications and traffic flows with network decisions and management.

In this thesis, we extend the solution proposed in the above-mentioned paper, creating a real-time network topology discovery application using POX controller, including link failures. The information gathered for the network is extracted in the form of tags that feeds the program of the previous ones.

To validate this solution, the Mininet emulator will be used. In the following chapters, we will explain in the needed depth the concepts of IoT, SDN, OpenFlow protocol that we will use, and the tools we will need in the practical part. Finally, we'll explain the implementation code.

---

**Code explanation**

I believe that the comments included within the code explain in great detail the function, so I will not include here more comments.

The only thing I will rewrite here is the way you can execute that script:

1. The path in Mininet you will need to put file topology.py is: /home/mininet/pox/
2. The path in Mininet you will need to put file topoDiscovery.py and configuration.json is: topoDiscovery.py: /home/mininet/pox/ext/
3. The command for executing topology.py is: sudo python /home/mininet/pox/topology.py
4. And the command for the second script from current dir /home/mininet/pox/ext/: ../pox.py topoDiscovery openflow.discovery forwarding.l2_learning

**Note:** The commands must be ran in separate terminals.

---

The final document of this Master Thesis can be found in this repository, but unfortunately is written in Greek. Maybe one day I will find some time to translate it. :)