#!/usr/bin/python

"""
Custom topology
This file creates a topology with 6 switches and 2 hosts.

     h1 --- s1 --- s2 --- s3 --- s6 --- h2
             \            /
              \          /
               s4 --- s5

/home/mininet/pox# sudo python type4_miniedit.py
"""
from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call

def myNetwork():

    net = Mininet( topo=None,
                   build=False,
                   ipBase='10.0.0.0/8',
                   link=TCLink, controller=None, autoStaticArp=True)

    info( '*** Adding controller\n' )
    c0=net.addController(name='c0',
                      controller=RemoteController,
                      ip='127.0.0.1',
                      protocol='tcp',
                      port=6633)

    info( '*** Add switches\n')
    s4 = net.addSwitch('s4', cls=OVSKernelSwitch, dpid='4')
    s6 = net.addSwitch('s6', cls=OVSKernelSwitch, dpid='6')
    s5 = net.addSwitch('s5', cls=OVSKernelSwitch, dpid='5')
    s1 = net.addSwitch('s1', cls=OVSKernelSwitch, dpid='1')
    s2 = net.addSwitch('s2', cls=OVSKernelSwitch, dpid='2')
    s3 = net.addSwitch('s3', cls=OVSKernelSwitch, dpid='3')

    info( '*** Add hosts\n')
    h1 = net.addHost('h1', cls=Host, ip='10.0.0.1', defaultRoute=None)
    h2 = net.addHost('h2', cls=Host, ip='10.0.0.2', defaultRoute=None)

    info( '*** Add links\n')
    net.addLink(h1, s1)
    net.addLink(s2, s1)
    net.addLink(s1, s4)
    net.addLink(s4, s5)
    net.addLink(s5, s3)
    net.addLink(s2, s3)
    net.addLink(s3, s6)
    net.addLink(s6, h2)

    info( '*** Starting network\n')
    net.build()
    info( '*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info( '*** Starting switches\n')
    net.get('s4').start([c0])
    net.get('s6').start([c0])
    net.get('s5').start([c0])
    net.get('s1').start([c0])
    net.get('s2').start([c0])
    net.get('s3').start([c0])

    info( '*** Post configure switches and hosts\n')
    s4.cmd('ifconfig s4 10.0.0.6')
    s6.cmd('ifconfig s6 10.0.0.8')
    s5.cmd('ifconfig s5 10.0.0.7')
    s1.cmd('ifconfig s1 10.0.0.3')
    s2.cmd('ifconfig s2 10.0.0.4')
    s3.cmd('ifconfig s3 10.0.0.5')

    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()

