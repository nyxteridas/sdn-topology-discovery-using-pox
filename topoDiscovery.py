""" 
Put this script under /home/mininet/pox/ext/
Command for execution:
Run the pox script with parameters topoDiscovery, openflow.discovery annd forwarding.l2_learning

/home/mininet/pox/ext# ../pox.py topoDiscovery openflow.discovery forwarding.l2_learning 
"""

from pox.core import core  
import pox.openflow.libopenflow_01 as of  
from pox.lib.revent import *  
from pox.lib.recoco import Timer  
from collections import defaultdict  
from pox.openflow.discovery import Discovery  
from pox.lib.util import dpid_to_str  
import time
import pox.host_tracker
import json

class topoDiscovery(EventMixin):

    # This variable will hold the count of the available links
    global linkCounter
    linkCounter = 0

    def __init__(self):
        def startup():
            core.openflow.addListeners(self, priority = 0)
            core.openflow_discovery.addListeners(self)
            """
            create a listener for host_tracker events with name HostEvent. 
            The method _handle_HostEvent which is implemented later, will handle those events
            """
            core.host_tracker.addListenerByName("HostEvent", self._handle_HostEvent)
        core.call_when_ready(startup, ('openflow','openflow_discovery'))
        print "init over"


    """ 
    below method will handle all the link events between switches 
    """
    def _handle_LinkEvent(self, event):

        """ 
        use of global variable linkCounter
        """
        global linkCounter

        """ 
        Hold the link which triggered the event 
        """
        l = event.link

        """ 
        Find out between which switches and ports was this link 
        """
        sw1 = l.dpid1
        sw2 = l.dpid2
        pt1 = l.port1
        pt2 = l.port2

        """
        Take a unique link representation in order to avoid duplicates
        """
        uniqueLinlRepr = l.uni

        """
        open configuration json for reading 
        and save it as dictionary to the variable jsonToPython. 
        Update the info according to what you are reading from network.
        Write it back to the configuration json.
        """
        with open('configuration.json', 'r') as myfile:
            jsonData=myfile.read().replace('\n', ' ')
        jsonToPython = json.loads(jsonData)

        """ 
        create the names of the switches 
        """
        s1Num = 's%d' %l.dpid1 
        s2Num = 's%d' %l.dpid2

        """ 
        read the part that contains the switches from the dictionary which was read previously from file, and store it 
        """
        switches = jsonToPython['switches']

        """ 
        Format the MAC address of switches and store it 
        """
        switches[s1Num] = '%s' %dpid_to_str(l.dpid1).replace('-',':')
        switches[s2Num] = '%s' %dpid_to_str(l.dpid2).replace('-',':')

        """ 
        update the element "switches" in the dictionary 
        which was read from the configuration JSON 
        """
        jsonToPython['switches'] = switches


        """ 
        read the part that contains the links from the dictionary which was read previously from file, and store it 
        """
        links = jsonToPython['link']

        """ 
        create the names of the switches based on the unique link representation
        """
        s1Num = 's%d' %uniqueLinlRepr.dpid1
        s2Num = 's%d' %uniqueLinlRepr.dpid2

        """ 
        If an event is added or removed, means that a link is up or down.
        Update all the relevant info in the configuration JSON        
        """
        if event.added:

            """
            Search in the existing list of link's if this one already exists
            If it exists, do nothing.
            """
            linkExistFlag = False

            for key,val in links.items():
                for key2,val2 in val.items():
                    if key2 == "connection":
                        if val2 == [s1Num,s2Num]:
                            linkExistFlag = True
            """
            if the link does not exist, give it a unique number
            and insert it in the list
            """
            if not linkExistFlag:
                linkCounter += 1
                links[format(linkCounter, 'b').zfill(10)] = {'connection': [s1Num,s2Num], 'ports': [uniqueLinlRepr.port1,uniqueLinlRepr.port2]}

        elif event.removed:

            """
            Search in the existing list of link's if this one already exists
            If it exists, remove it.
            """

            for key,val in links.items():
                for key2,val2 in val.items():
                    if key2 == "connection":
                        if val2 == [s1Num,s2Num]:
                            del links[key]

        """ 
        update the element "link" in the dictionary 
        which was read from the configuration JSON 
        """
        jsonToPython['link'] = links


        """ Write back to the configuration json the updates. """
        with open('configuration.json', 'w') as outfile:
           json.dump(jsonToPython, outfile, indent=4, separators=(',', ': '), sort_keys=True)


    """ 
    below method will listen to host_tracker events, fired up every time a host is detected. 
    To fire up we must issue a pingall from mininet cli.         
    """
    def _handle_HostEvent(self, event):
        
        """ 
        use of global variable linkCounter
        """
        global linkCounter

        """
        In our case we need to know which host is with us, 
        which is its MAC, in which switch it is connected,
        and in which port  
        """
        macaddr = event.entry.macaddr.toStr()
        port = event.entry.port
        dp = event.entry.dpid

        """
        open configuration json for reading 
        and save it as dictionary to the variable jsonToPython. 
        Update the info according to what you are reading from network.
        Write it back to the configuration json.
        """
        with open('configuration.json', 'r') as myfile:
            jsonData=myfile.read().replace('\n', ' ')
        jsonToPython = json.loads(jsonData)

        """ 
        read the part that contains the hosts from the dictionary which was read previously from file, and store it 
        """
        hosts = jsonToPython['hosts']

        """ 
        create the names of the hosts 
        """
        hNum = 'h%d' %port 

        """ 
        create the names of the switches 
        """
        sNum = 's%d' %dp 

        """ 
        Format the entry of a host 
        """
        hosts[hNum] = {"mac": macaddr, "ip": "0.0.0.0", "color": "red"}

        """ 
        update the element "hosts" in the dictionary 
        which was read from the configuration JSON 
        """
        jsonToPython['hosts'] = hosts

        """ 
        update the list of links with those from hosts 
        read the part that contains the links from the dictionary which was read previously from file, and store it 
        """
        links = jsonToPython['link']

        """
        Search in the existing list of link's if this one already exists
        If it exists, do nothing.
        """
        linkExistFlag = False

        for key,val in links.items():
            for key2,val2 in val.items():
                if key2 == "connection":
                    if val2 == [hNum,sNum]:
                        linkExistFlag = True

        """
        if the link does not exist, give it a unique number
        and insert it in the list
        """
        if not linkExistFlag:
            linkCounter += 1
            links[format(linkCounter, 'b').zfill(10)] = {'connection': [hNum,sNum], 'ports': [1,port]}      

        """ 
        Write back to the configuration json the updates. 
        """
        with open('configuration.json', 'w') as outfile:
           json.dump(jsonToPython, outfile, indent=4, separators=(',', ': '), sort_keys=True)



def launch():  
    from host_tracker import launch 
    launch()

    """ Initialize the JSON configuration file """
    with open('configuration.json', 'r') as myfile:
            jsonData=myfile.read().replace('\n', ' ')
    jsonToPython = json.loads(jsonData)
    jsonToPython['switches'] = {}
    jsonToPython['hosts'] = {}
    jsonToPython['link'] = {}
    with open('configuration.json', 'w') as outfile:
           json.dump(jsonToPython, outfile, indent=4, separators=(',', ': '), sort_keys=True)

    core.registerNew(topoDiscovery)
